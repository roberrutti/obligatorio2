//
//  CustomLocationManager.swift
//  SegundoObligatorio
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 2/6/16.
//  Copyright © 2016 Universidad Catolica. All rights reserved.
//

import Foundation
import MapKit


public class CustomLocationManager
{
    
    
    static func customLocationEnabled()->Bool
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if(!(defaults.objectForKey("LocalizationEnabled")==nil)&&defaults.objectForKey("LocalizationEnabled") as! String == "off"){
            return true
        }
        
        return false
    }
    
    
    static func saveCustomCoords(coords:CLLocationCoordinate2D)->Void
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        let lat = NSNumber(double: coords.latitude)
        let lon = NSNumber(double: coords.longitude)
        let userLocation: NSDictionary = ["lat": lat, "long": lon]
        defaults.setObject(userLocation, forKey: "userCustomLocation")
        
    }
    
    
    static func getCustomCoords() -> CLLocationCoordinate2D?{
        let dic = NSUserDefaults.standardUserDefaults().objectForKey("userCustomLocation") as? [String:CLLocationDegrees]
        if let dic = dic{
            return CLLocationCoordinate2D(latitude: dic["lat"]!, longitude: dic["long"]!)
        }else{
            return nil
        }
    }
    
    
    
    
}