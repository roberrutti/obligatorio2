//
//  HomeViewController.swift
//  SegundoObligatorio
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 2/6/16.
//  Copyright © 2016 Universidad Catolica. All rights reserved.
//

import UIKit
import SwiftLocation
import DateTools

class HomeViewController: UIViewController {
    
    @IBOutlet weak var weatherIconLabel: UILabel!
    @IBOutlet var cityName: UILabel!
    @IBOutlet var temperature: UILabel!
    
    var dayToFormat: NSDate?
    var forecastsObtained:Forecast!
    var myMutableString = NSMutableAttributedString()
    
    
    @IBOutlet weak var forecastsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        self.getForecasts()
        self.forecastsCollectionView.reloadData()
        if self.forecastsObtained != nil
        {
            self.temperature.attributedText=self.temperatureToString(Double((forecastsObtained.list?[0].temperature?.description)!)!,isInCollectionView: false)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getForecasts()->Void
    {
        APIClient.sharedClient.obteinCoords{() -> Void in
            APIClient.sharedClient.weatherOnCompletion { (forecasts, error) -> Void in
                
                if let forecasts = forecasts {
                    
                    self.forecastsObtained = forecasts
                    
                    self.cityName.text = forecasts.name
                    
                    self.temperature.attributedText=self.temperatureToString(Double((forecasts.list?[0].temperature?.description)!)!,isInCollectionView: false)
                    
                    
                    let condition=self.forecastsObtained.list?[0].weatherList![0].id
                    let iconString=self.forecastsObtained.list?[0].weatherList![0].icon
                    self.weatherIconLabel.text = WeatherIcon(condition: condition!,iconString:iconString!).iconText
                    
                    
                    self.forecastsCollectionView.reloadData()
                    
                }
            }
            
        }
    }
    
    
    
    private struct Storyboard
    {
        static let CellIdentifier = "ForecastCell"
    }
    
    
    func celsiusToFahrenheit(celsiusTemp:Double)->Double
    {
        return ((celsiusTemp*(9/5)+32))
    }
    
    func temperatureToString(celsiusTemp:Double, isInCollectionView:Bool)->NSMutableAttributedString
    {
        
        var resultString:String
        if(preferCelsius())
        {
            let celsiusString=Int(celsiusTemp).description
            
            resultString=celsiusString+"ºC"
            
            
        }else{
            let fahrenheitString = Int(celsiusToFahrenheit(celsiusTemp)).description
            
            resultString=fahrenheitString+"ºF"
        }
        
        if(isInCollectionView)
        {
            let font:UIFont? = UIFont(name: "Helvetica", size:25)
            let fontSuper:UIFont? = UIFont(name: "Helvetica", size:15)
            let attString:NSMutableAttributedString = NSMutableAttributedString(string: resultString, attributes: [NSFontAttributeName:font!])
            
            if(attString.string.characters.count <= 3)
            {
                attString.setAttributes([NSFontAttributeName:fontSuper!,NSBaselineOffsetAttributeName:10], range: NSRange(location:1,length:2))
                
            }else{
                attString.setAttributes([NSFontAttributeName:fontSuper!,NSBaselineOffsetAttributeName:10], range: NSRange(location:2,length:2))
                
            }
            
            
            return attString;
            
            
        }else{
            let font:UIFont? = UIFont(name: "Helvetica", size:50)
            let fontSuper:UIFont? = UIFont(name: "Helvetica", size:25)
            let attString:NSMutableAttributedString = NSMutableAttributedString(string: resultString, attributes: [NSFontAttributeName:font!])
            
            if(attString.string.characters.count <= 3)
            {
                attString.setAttributes([NSFontAttributeName:fontSuper!,NSBaselineOffsetAttributeName:10], range: NSRange(location:1,length:2))
                
            }else{
                attString.setAttributes([NSFontAttributeName:fontSuper!,NSBaselineOffsetAttributeName:10], range: NSRange(location:2,length:2))
                
            }
            return attString;
            
            
        }
        
    }
    
    func preferCelsius() -> Bool
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if(defaults.objectForKey("Metric")==nil || defaults.objectForKey("Metric") as! String=="celsuis")
        {
            return true
        }
        return false
        
    }
    
    
    
    
    
}

extension HomeViewController:UICollectionViewDataSource
{
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        if self.forecastsObtained != nil
        {
            
            return 1
        }
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.forecastsObtained != nil
        {
            
            return (forecastsObtained.list?.count)!
        }
        return 0
    }
    
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Storyboard.CellIdentifier, forIndexPath: indexPath) as! ForecastCollectionViewCell
        
        if self.forecastsObtained != nil
        {
            
            cell.temperatureLabel.attributedText = self.temperatureToString(Double((forecastsObtained.list?[indexPath.item].temperature?.description)!)!,isInCollectionView: true)
            let condition=self.forecastsObtained.list?[indexPath.item].weatherList![0].id
            let iconString=self.forecastsObtained.list?[indexPath.item].weatherList![0].icon
            cell.weatherIconLabel.text = WeatherIcon(condition: condition!,iconString:iconString!).iconText
            
            self.dayToFormat = (self.forecastsObtained.list?[indexPath.item].date)!
            cell.dayLabel.text=self.self.dayToFormat?.formattedDateWithFormat("EEE")
            
        }
        
        
        
        return cell
    }
    
}


