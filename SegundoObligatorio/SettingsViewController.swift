//
//  SettingsViewController.swift
//  SegundoObligatorio
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 31/5/16.
//  Copyright © 2016 Universidad Catolica. All rights reserved.
//

import UIKit
import MapKit


class SettingsViewController: UITableViewController
{


    @IBOutlet weak var unitsSegmentControl: UISegmentedControl!
    
    @IBOutlet var mapCell: UITableViewCell!
    @IBOutlet var switchLocation: UISwitch!
    @IBOutlet var mapWithCoords: MKMapView!
    
    
    var dropPin: MKPointAnnotation?
    var customCoords: CLLocationCoordinate2D?

    
    override func viewDidLoad() {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if(!(defaults.objectForKey("Metric")==nil) && defaults.objectForKey("Metric") as! String=="faranheit")
        {
            unitsSegmentControl.selectedSegmentIndex=1
        }
        if(CustomLocationManager.customLocationEnabled())
        {
            switchLocation.setOn(false, animated: false)
            mapCell.hidden = false
            setPinLocation(CustomLocationManager.getCustomCoords()!)
        }
            
        else
        {
            switchLocation.setOn(true, animated: false)
            mapCell.hidden = true
        }
        
     

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func onPressSaveButton(sender: AnyObject) {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        switch unitsSegmentControl.selectedSegmentIndex {
        case 0:
            defaults.setObject("celsuis", forKey: "Metric")
        case 1:
            defaults.setObject("faranheit", forKey: "Metric")
        default:
            defaults.setObject("celsuis", forKey: "Metric")
        }
        
        if(switchLocation.on)
            
        {
            defaults.setObject("on", forKey: "LocalizationEnabled")
        }
        else{
            if(!(customCoords == nil))
            {
                CustomLocationManager.saveCustomCoords(customCoords!)
                
            }
            defaults.setObject("off", forKey: "LocalizationEnabled")
            defaults.setObject("off", forKey: "LocalizationEnabled")
            
        }
        
        dismissViewControllerAnimated(true) { 
            
        }

    }
    
    @IBAction func cancelButtonAction(sender: AnyObject) {
        
        
        dismissViewControllerAnimated(true) {
            
        }

    }
    
    
    @IBAction func switchLocationChange(sender: AnyObject) {
        
        if (switchLocation.on){
        
            mapCell.hidden = true

        }
        
        else{
        
            mapCell.hidden = false
        }
    }
    
    
    func saveCustomCoords(coords:CLLocationCoordinate2D)->Void
    {
        customCoords=coords
        setPinLocation(coords)
        
    }
    
    @IBAction func locationTappedOnMap(sender: AnyObject) {
        
        let coord = mapWithCoords.convertPoint(sender.locationInView(mapWithCoords), toCoordinateFromView: mapWithCoords)
        saveCustomCoords(coord)
        setPinLocation(coord)
    }
    
    func setPinLocation(coords:CLLocationCoordinate2D)->Void
    {
        if(dropPin==nil)
        {
            dropPin  = MKPointAnnotation()
        }
        dropPin!.coordinate = coords
        mapWithCoords.addAnnotation(dropPin!)
        
        
    }

    
    

}
