//
//  List.swift
//  SegundoObligatorio
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 26/5/16.
//  Copyright © 2016 Universidad Catolica. All rights reserved.
//

import Foundation
import ObjectMapper

class List: Mappable{
    var temperature: Float?
    var weatherList: [Weather]?
    var date: NSDate?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        self.temperature <- map["temp.day"]
        self.date <- (map["dt"], DateTransform())
        self.weatherList <- map["weather"]
        
    }
}