//
//  APIClient.swift
//  SegundoObligatorio
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 23/5/16.
//  Copyright © 2016 Universidad Catolica. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper
import SwiftLocation

class APIClient {
    
    static let sharedClient = APIClient()
    
    var latitude: String = ""
    var longitude: String = ""
    
    
    private let baseURL = "http://api.openweathermap.org/data/2.5/forecast/"
    
    private init() {
        
    }
    
    func obteinCoords(onCompletion:()-> Void){
        
        if(CustomLocationManager.customLocationEnabled())
        {
            
            try! SwiftLocation.shared.currentLocation(Accuracy.House, timeout: 22, onSuccess: { (location) in
                self.latitude = (CustomLocationManager.getCustomCoords()?.latitude.description)!
                self.longitude = (CustomLocationManager.getCustomCoords()?.longitude.description)!
                
                onCompletion()
                })
            { (error)-> Void in
                self.displayWarning("Advertencia", mainContent: "No se pudo obtener coordenadas, verificar que esten habilitadas en ajustes", buttonContent: "Aceptar")
            }
            
        }else{
            try! SwiftLocation.shared.currentLocation(Accuracy.House, timeout: 22, onSuccess: { (location) in
                self.latitude = (location?.coordinate.latitude.description)!
                self.longitude = (location?.coordinate.longitude.description)!
                
                onCompletion()
                })
            { (error)-> Void in
                self.displayWarning("Advertencia", mainContent: "No se pudo obtener coordenadas, verificar que esten habilitadas en ajustes", buttonContent: "Aceptar")
            }
            
            
        }
        
    }
    
    func weatherOnCompletion(onCompletion: (forecasts: Forecast?, error: NSError?) -> Void) {
        
        
        Alamofire.request(.GET, self.baseURL + "/daily?lat="+latitude+"&lon="+longitude+"&cnt=7&mode=json&units=metric&appid=c55676d50e2607e2eaf6e9ff6dbca44f").validate().responseJSON { (response:
            Response<AnyObject, NSError>) -> Void in
            print(response)
            switch response.result {
                
            case .Failure(let error):
                onCompletion(forecasts: nil, error: error)
                self.displayWarning("Error", mainContent: "No se pudo obtener los datos del tiempo, verificar que se cuente con conexión a internet", buttonContent: "Aceptar")

            case .Success(let value):
                if let forecasts = Mapper<Forecast>().map(value) {
                
                    
                    onCompletion(forecasts: forecasts, error: nil)
                }else {
                    onCompletion(forecasts:  nil, error: NSError(domain: "MyApp", code: 9999, userInfo: [NSLocalizedDescriptionKey: "Fallo el mapeo"]))
                }
            }
            
        }
    }
    
    func displayWarning(title:String,mainContent:String,buttonContent:String)-> Void
    {
        let alert = UIAlertView()
        alert.title = title
        alert.message = mainContent
        alert.addButtonWithTitle(buttonContent)
        alert.show()
        
    }
    
    }