//
//  Weather.swift
//  SegundoObligatorio
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 26/5/16.
//  Copyright © 2016 Universidad Catolica. All rights reserved.
//

import Foundation
import ObjectMapper

class Weather: Mappable{
    var id: Int?
    var icon: String?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        self.id <- map["id"]
        self.icon <- map["icon"]
        
    }
}