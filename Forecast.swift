//
//  Forecast.swift
//  SegundoObligatorio
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 23/5/16.
//  Copyright © 2016 Universidad Catolica. All rights reserved.
//

import Foundation
import ObjectMapper

class Forecast: Mappable {
    
    var name: String?
    var list: [List]?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        self.name <- map["city.name"]
        self.list <- map["list"]

    }
    
}